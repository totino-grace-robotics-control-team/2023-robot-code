/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity
 */
public final class Constants {

    /**
     * track which PWM port on the roborio is used for which control
     */
    public static final class pwmPortConstants {
        // drive motor
        public static final int kLeftDrivetrainMotors = 0;
        public static final int kRightDrivetrainMotors = 1;

        public static final int reachMotor = 4;
        public static final int tiltMotor = 2;
        public static final int clawMotor = 3;
      //  public static final int wristMotor = 5;

    }

    /**
     * track which DIO port on the roborio is used for which sensor
     */
    public static final class dioPortConstants {
        /*dio ports 0, 1, 2, 3 are going to be used for encoder */
      //  public static final int m_verticalLowerLimitSwitch = 9;
        // public static final int clawBackwardLimitSwitch = 8;
        public static final int m_verticalHigherLimitSwitch = 6;
        public static final int clawSensor = 7;
        public static final int extendLimitSwitchPort = 5;
        public static final int retractLimitSwitchPort = 4;


        // public static final int[] kLeftDriveEncoder = new int[]{0, 1};
        // public static final int[] kRightDriveEncoder = new int[]{2, 3};
        // public static final int[] kPeriscopeEncoder = new int[]{4, 5};
    }

    /**
     * USB ports, both on the roborio and the driver station computer
     */
    public static final class usbPortConstants {
        // RoboRio USB ports
        public static final int kShooterCamera = 0;
        public static final int kIntakeCamera = 1;

        // Computer USB ports
        public static final int kDriveJoystick = 0;
        // public static final int kWeaponsJoystick = 1;
        public static final int kfightStick = 1;
    }

    /**
     * button assignments for the drive joystick
     */
    public static final class DriveStickButtons {
        // Drive Joystick
        public static final int kToggleDriveButton = 1;
        // public static final int kToggleCameraButton = 2;
        //public static final int kArmsDownButton = 3;
        //public static final int kArmsExtendButton = 4;
        //public static final int kArmsUpButton = 5;
        //public static final int kArmsRetractButton = 6;
        //public static final int kClawCloseCube = 9;
        //public static final int kClawCloseCone = 10;
        //public static final int kClawOpen = 11;
        // below buttons are not used currently -- open to change
        public static final int kAimFixedN8Button = 12;
        int time = 3; //subject to change
    }

    /**
     * Button assignments on the weapons Joystick
     */
    public static final class WeaponsJoystickButtons {

      //  public static final int kIcsIntakeButton = 10;
        //public static final int kIcsShootButton = 7;
        //public static final int kIcsReverseButton = 9;
        //public static final int kIcsCancelButton = 2;

    }

    // public static final class WeaponsConstants {
    //     // Weapons Joystick

    //     public static final int kpuncherRetractButton = 3; // B
    //     // public static final int kRaiseElevatorButton = 4; // Y
    //     // public static final int kLowerElevatorButton = 2; //A
    //     public static final int kIntakeButton = 7; // Left Trigger
    //     public static final int kShootButton = 8; // Right Trigger
    //     public static final int kPerformClimb = 1; // X
    //     public static final int kCancelIntake = 12;
    //     // public static final int kTightenStringButton = 4; //Y
    //     public static final int kReleaseStringButton = 2;// A
    //     public static final int kTightenStringButton = 4;// Y
    //     // public static final int kIcsReverseButton = 9;
    //     // public static final int kIcsIntakeButton = 10;
    // }

    /**
     * Button assignments on the flightstick
     */
    public static final class FightStickButton {
        public FightStickButton(Joystick m_weaponsJoystick, int buttonNumber) {
        }
        public static final int xButton = 1; // aButton = purple
        public static final int aButton = 2; // bButton = red
        public static final int bButton = 3; // xButton = pink
        public static final int yButton = 4; // yButton = cyan
        public static final int L1Lb = 5; // L1Lb = top gray
        public static final int R1Rb = 6; // R1Rb = top right gray
        public static final int L2LT = 7; // share = top button, 4 away from right
        public static final int R2RT = 8; // options = top button, 3 away from right
        public static final int R3 = 12; // far right button on top
        // there is another button on the fightstick that is really glitchy on the driverstation, button L3/SL should be with 9

    }

    public static final class subsystemEnablement {
        public static final boolean kVisionEnable = false;
    }

    /**
     * flags to enable the debug messages for each subsystem
     */
    public static final class subsystemDebuggers {
        public static final boolean kVisionDebug = false;
        public static final boolean kDrivetrainDebug = false;
        public static final boolean kClimberDebug = false;
        public static final boolean kIcsDebug = true;
        public static final boolean kConveyDebug = false;
        public static final boolean kPneumaticsDebug = false; // TODO turn this off for competition
        public static final boolean kArmSubsystemDebug = true;
    }

    public static final class drivetrainConstants {
        public static final int kEncoderCPR = 256; // CPR is Counts per Revolution
        public static final double kWheelDiameterMeters = 0.15;

        public static final double kEncoderDistancePerPulse = (kWheelDiameterMeters * Math.PI) / (double) kEncoderCPR;
    }

    public static final class visionConstants {
        public static final int[] kCameraResolution = new int[] { 320, 240 };
        public static final int kCameraFPS = 15;
    }

    // public static final class climberConstants {
    //     public static final double kWinchInSpeed = 0.5;
    //     public static final double kWinchOutSpeed = -1.0;
    //     public static final double kRaiseElevatorSpeed = 0.5;
    //     public static final double kLowerElevatorSpeed = -0.5;
    //     public static final double kReleaseStringSpeed = -0.5;
    //     public static final double kTightenStringSpeed = 0.5;
    //     // public static final double kReleaseStringSpeed = 0.2;
    // }

    // public static final class icsConstants {
    //     public static double kIntakeMotorSpeed = 0;
    //     public static double kConveyorMotorSpeed = 0;
    //     public static double kShooterMotorSpeed = 0;

    // }

    // public static final class conveyConstants {
    //     public static double kConveyorMotorSpeed = 0;
    // }

    public static final class aimConstants {
        public static double kClicksPer360Degrees = (360.0 / 2048.0);
    }

    // public static final class intakeConstants {
    //     public static double kIntakeMotorSpeed = 0;
    // }

    // public static final class shootConstants {
    //     public static double kShooterMotorSpeed = 0;
    // }

    // public static final class CwmConstants {
    //     public static final int kDeployMotorPort_PWM = 6;
    //     public static final int kSpinMotorPort_PWM = 7;

    //     public static final int[] kEncoderPort = new int[] { 4, 5 };

    //     public static final boolean kRequireCalibrationDuringMatch = false;
    //     public static final boolean kShowDebugLogs = false;

    //     public static final double DISK_DEPLOY_SPEED = 0.7;
    //     public static final double DISK_SPIN_SPEED = 1;
    // }

}
