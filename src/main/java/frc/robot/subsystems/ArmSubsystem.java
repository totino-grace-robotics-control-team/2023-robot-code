package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.motorcontrol.Spark;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Debug;
import frc.robot.Constants.FightStickButton;
import frc.robot.Constants.dioPortConstants;
import frc.robot.Constants.pwmPortConstants;
import frc.robot.Constants.subsystemDebuggers;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;


/**
 * subsystem for the lift/grab/reach arm system
 */
public class ArmSubsystem extends SubsystemBase {
    private final Debug m_debugger = new Debug("ArmSubsystem ");
    /* up, down, retract, extract */
    private final Spark m_clawMotor = new Spark(pwmPortConstants.clawMotor);
    // private final Spark m_tiltMotor = new Spark(pwmPortConstants.tiltMotor);
    
    private static final int deviceID = 1;
    private static final MotorType m_MotorType = MotorType.kBrushless;

    private final CANSparkMax m_tiltMotor = new CANSparkMax(deviceID, m_MotorType);
    

    private final Spark m_reachMotor = new Spark(pwmPortConstants.reachMotor);
  //  private final Spark m_wristMotor = new Spark(pwmPortConstants.wristMotor);
  //  private final DigitalInput m_verticalLowerLimitSwitch = new DigitalInput(
    //        dioPortConstants.m_verticalLowerLimitSwitch);
    private final DigitalInput m_verticalHigherLimitSwitch = new DigitalInput(
            dioPortConstants.m_verticalHigherLimitSwitch);
    private final DigitalInput m_stopRetractingLimitSwitch = new DigitalInput(
            dioPortConstants.retractLimitSwitchPort);
    private final DigitalInput m_stopExtendingLimitSwitchDigitalInput = new DigitalInput(
            dioPortConstants.extendLimitSwitchPort);

    public ArmSubsystem() {
        super();
        // Should we show logs?
        if (subsystemDebuggers.kArmSubsystemDebug) {
            m_debugger.enable();
        }
    }

    @Override
    public void periodic()
    {
        /**
     * There are several useful bus measurements you can get from the SparkMax.
     * This includes bus voltage (V), output current (A), Applied Output 
     * (duty cycle), and motor temperature (C)
     */
    double busVoltage = m_tiltMotor.getBusVoltage();
    double current = m_tiltMotor.getOutputCurrent();
    double appliedOut = m_tiltMotor.getAppliedOutput();
    double temperature = m_tiltMotor.getMotorTemperature();

    // Open SmartDashboard when your program is running to see the values
    SmartDashboard.putNumber("Bus Voltage", busVoltage);
    SmartDashboard.putNumber("Current", current);
    SmartDashboard.putNumber("Applied Output", appliedOut);
    SmartDashboard.putNumber("Motor Temperature", temperature);
    }

    // private static double TILTMOTORSPEED = 0.2;

    /**
     * move arm up
     * move arm down
     * extract the arm
     * retract the arm
     */
    // public void moveUp() {
    //     m_tiltMotor.set(0.4);
    //     SmartDashboard.putNumber("Tilt motor", -0.4);

    // } @INFO Changed for Spark Max Controller

    public void moveUp() {
        //m_tiltMotor.set(tiltJoystick.getZ());
        m_tiltMotor.set(1);
    }

    public void moveDown() {
        m_tiltMotor.set(-0.1);
     //   m_debugger.log("vertical motor is working!!");
        SmartDashboard.putNumber("Tilt motor", 0.1);

    }

    private static double tiltMotorHold = -0.015;
    public void reachManual(Joystick fightJoystick) {
    //      double target = fightJoystick.getY();

    //     // move at different rate depending on up or down
    //   //  System.out.println("arm joystick is working");
    //    double speed = target/= 1.5;
    //    m_reachMotor.set(speed);
    //     SmartDashboard.putNumber("Reach motor", speed);

        
        double getY = fightJoystick.getY();
        double tiltSpeed = fightJoystick.getY()*0.15;
        SmartDashboard.putNumber("Y-Axis Tilt", getY);
        if(tiltSpeed<0 && this.verticalHigherLimitSwitch()) {
            tiltSpeed = tiltMotorHold;
        }
        if(tiltSpeed==0) {
            tiltSpeed = tiltMotorHold;
        }
        m_tiltMotor.set(tiltSpeed);
        
    } 
    

        // check if the limit switch is reached and moving TOWARDS that limit switch.
      /*   if (getVerticalLowerLimitSwitch() && target > 0) { // backwards
            target = 0.0;
            System.out.println("upper limit switch val = 0");
        } else if (getVerticalHigherLimitSwitch() && target < 0) {
            target = 0.0;
            System.out.println("lower limit switch val = 0");
        }
        */

     //   if (getVerticalHigherLimitSwitch() && target < 0) {
       //     target = 0.0;
        //    System.out.println("lower limit switch val = 0");
        //}

       /*  m_tiltMotor.set(target);
        if (target != 0) {
            System.out.println(target);
        }
        */
    

  //  public boolean getVerticalHigherLimitSwitch() {
   //     return m_verticalHigherLimitSwitch.get();
   // }

    public void stopMovingVertically() {
        m_tiltMotor.stopMotor();
        SmartDashboard.putNumber("Tilt Motor", 0.0);

    }

    /*public boolean getVerticalLowerLimitSwitch() {
      //  m_debugger.log(String.valueOf(!m_verticalLowerLimitSwitch.get()));
        return !m_verticalLowerLimitSwitch.get();

    }
    */

    // logic for reach - extend, retract etc.

    public void extract() {
        m_reachMotor.set(-0.6);
        SmartDashboard.putNumber("Reach motor", 0.1);

    }

    public void stopExtending() {
        m_reachMotor.stopMotor();
        SmartDashboard.putNumber("Reach motor", 0.0);

    }

    public void retract() {
        m_reachMotor.set(0.5);
        SmartDashboard.putNumber("Reach motor", -0.5);

    }
    public boolean isRetractedFully() {
        return !m_stopRetractingLimitSwitch.get();
    }
    public boolean isExtendedFully() {
        return !m_stopExtendingLimitSwitchDigitalInput.get();
    }
    public void stopRetracting() {
        m_reachMotor.set(0.0);
        SmartDashboard.putNumber("Reach motor", 0.0);
    }
    



    // claw commands below!! enjoy

    public void open() {
        // TODO: put arm move here
        m_clawMotor.set(-1.0);

       // SmartDashboard.putNumber("Claw Motor", -0.5);
       SmartDashboard.putNumber("Claw Motor", -1.0);

    }

    public void closeClawCube() {
        m_clawMotor.set(1.0);
        SmartDashboard.putNumber("Claw Motor", 1.0);
        
        // TODO: put arm move here
    }

    public void closeClawCone() {
        // TODO: put arm move here
        m_clawMotor.set(1.0);
        SmartDashboard.putNumber("Claw Motor", 0.3);

    }

    public void stopMovingClawMotor() {
        m_clawMotor.stopMotor();
      //  SmartDashboard.putNumber("Claw Motor", 0);
      SmartDashboard.putNumber("Claw Motor", 0.0);


    }
    public boolean verticalHigherLimitSwitch() {   
        return !(m_verticalHigherLimitSwitch.get());
    }
    // public void wristUp () {
    // TODO: put arm move here
    // }
    // public void wristDown () {
    // TODO: put arm move here
    // }
}
