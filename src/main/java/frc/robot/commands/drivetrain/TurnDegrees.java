package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class TurnDegrees extends CommandBase{
    private final Drivetrain m_driveTrain;
	// private double startTime;
	private double m_AutoDriveSpeed;
	

	public TurnDegrees(Drivetrain subsystem, double DrivetrainSpeed) {
		//System.out.println("AutoDrive (command): Constructed");
		m_AutoDriveSpeed = DrivetrainSpeed;

		m_driveTrain = subsystem;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_driveTrain);

	}
    @Override
	public void initialize() {
		System.out.println("Before delay auto thing");
		//Timer.delay(1);
		// startTime = System.currentTimeMillis();
		//System.out.println("After delay auto thing");
	}

	// Called every time the scheduler runs while the command is scheduled.
	@Override
	public void execute() {
		m_driveTrain.drive(0.0, -0.7);
		System.out.println("moving the robot");
		
	}

	// Called once the command ends or is interrupted.
	@Override
	public void end(boolean interrupted) {
		//System.out.println("Ended auto thing, interrupted?" + interrupted);
		m_driveTrain.drive(0, 0);
		System.out.println("ending the moving the robot");
	}

	// Returns true when the command should end.
	@Override
	public boolean isFinished() {
		// if (System.currentTimeMillis() >= (startTime + Constants.AutonomousConstants.kAutoMoveTime)){
		// //	System.out.println("auto done");
		// 	return true;
		// } else {
		// 	return false;
		// }

		//Temporarily doing nothing, since this is going to be overhauled pretty soon anyway
		return false;
	}
}
