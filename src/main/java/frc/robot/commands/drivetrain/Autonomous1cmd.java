package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.ArmCommands.ArmsDown;
import frc.robot.commands.ArmCommands.ArmsExtract;
import frc.robot.commands.ArmCommands.ArmsRetract;
import frc.robot.commands.ArmCommands.ArmsUp;
import frc.robot.commands.ArmCommands.ClawOpen;
import frc.robot.subsystems.ArmSubsystem;

public class Autonomous1cmd extends SequentialCommandGroup{
         public Autonomous1cmd(ArmSubsystem aS) {
             // Add your commands in the super() call, e.g.
             // super(new FooCommand(), new BarCommand());
             super(new ArmsDown(aS).withTimeout(3), new ArmsExtract(aS).withTimeout(3), new ArmsDown(aS).withTimeout(1), new ClawOpen(aS).withTimeout(1), new ArmsRetract(aS));
    
    
        
         }
    
    
    }
