package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.Drivetrain;


public class Autonomous2cmd extends SequentialCommandGroup{
         public Autonomous2cmd(ArmSubsystem aS, Drivetrain dT) {
             // Add your commands in the super() call, e.g.
             // super(new FooCommand(), new BarCommand());
              //super(new ArmsDown(aS).withTimeout(3), new ArmsExtract(aS).withTimeout(3), new ArmsDown(aS).withTimeout(1), new ClawOpen(aS).withTimeout(1), new ArmsRetract(aS));
            //super (new DriveTrain(aS), );
             super (new AutoDrive(dT, -0.64).withTimeout(0.25), 
             new AutoDrive(dT, 0.5).withTimeout(2),
             new AutoDrive(dT, -0.55).withTimeout(4)); 
             //new AutoDrive(dT, 0.4).withTimeout(1), 
             //new AutoDrive(dT, 0.4).withTimeout(2),
             //new AutoDrive(dT, 0.4).withTimeout(4));
             
           //  new TurnDegrees(dT, 0).withTimeout(1), 
           //  new AutoDrive(dT, 0.7).withTimeout(3)
     
        
         }
    
    
    }
