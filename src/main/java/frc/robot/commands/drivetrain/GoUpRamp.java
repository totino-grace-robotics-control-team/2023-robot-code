package frc.robot.commands.drivetrain;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class GoUpRamp extends CommandBase{
	private final Drivetrain m_Drivetrain;

	public GoUpRamp(Drivetrain subsystem) {
		System.out.println("ArmsDown (command): Constructed");
		m_Drivetrain = subsystem;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_Drivetrain);
	}
    @Override
	public void initialize() {
        m_Drivetrain.moveRobot(.2);
	}

	@Override
	public void execute() {
		// call moveDown;
	}

	@Override
	public void end(boolean interrupted) {
        m_Drivetrain.stopMoving();
        System.out.println("up ramp cmd end");
	}

	@Override
	public boolean isFinished() {
        return m_Drivetrain.isBalanced();
	}
	
    
}
