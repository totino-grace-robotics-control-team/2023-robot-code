package frc.robot.commands.ArmCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem; 

/**
 * Command for raising the arms 
 */
public class ArmsUp extends CommandBase {
	private final ArmSubsystem m_ArmSubsystem;

	public ArmsUp(ArmSubsystem subsystem) {
		// System.out.println("AutoDrive (command): Constructed");
		m_ArmSubsystem = subsystem;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_ArmSubsystem);
	}

	@Override
	public void initialize() {
		System.out.println("Before delay auto thing");

	}

	 @Override
	 public void execute() {
	 	// System.out.println("moving the robot");
	 	m_ArmSubsystem.moveUp();
	 }

	@Override
	public void end(boolean interrupted) {
		// System.out.println("ending the moving the robot");
		m_ArmSubsystem.stopMovingVertically();
	}

	@Override
	public boolean isFinished() {
		return m_ArmSubsystem.verticalHigherLimitSwitch();
	}
}
