package frc.robot.commands.ArmCommands;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.ArmSubsystem;



public class TestNewCommands extends SequentialCommandGroup{
// // NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// // information, see:
// // https://docs.wpilib.org/en/latest/docs/software/commandbased/convenience-features.html
// 	/**
// 	 * Creates a new IntakeProcess.
// 	 */
  
 	public TestNewCommands(ArmSubsystem aS) {
 		// Add your commands in the super() call, e.g.
 		// super(new FooCommand(), new BarCommand());
 		super(new ArmsDown(aS), new WaitCommand(5), new ArmsUp(aS));


	
 	}


}
