package frc.robot.commands.ArmCommands;

import java.sql.Time;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;

public class ArmsExtract extends CommandBase {
	private static final int Time = 0;
	private final ArmSubsystem m_ArmSubsystem;
	public ArmsExtract(ArmSubsystem subsystem) {
		//System.out.println("AutoDrive (command): Constructed");
		m_ArmSubsystem = subsystem;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_ArmSubsystem);
	}

    
    @Override
	public void initialize() {
		System.out.println("Before delay auto thing");
		//TODO: figure out how to do time commands (wait time but tele-op)

	}

	@Override
	public void execute() {
		// System.out.println("moving the robot");
		//while (Time < 3000) {
			
		//}
		m_ArmSubsystem.extract();
	}

	@Override
	public void end(boolean interrupted) {
		// System.out.println("ending the moving the robot");
		m_ArmSubsystem.stopExtending();
	}

	@Override
	public boolean isFinished() {
		
		return false;
	}
}


