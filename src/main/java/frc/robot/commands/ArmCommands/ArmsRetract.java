package frc.robot.commands.ArmCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;

public class ArmsRetract extends CommandBase{
	private final ArmSubsystem m_ArmSubsystem;
	public ArmsRetract(ArmSubsystem subsystem) {
		//System.out.println("AutoDrive (command): Constructed");
		m_ArmSubsystem = subsystem;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_ArmSubsystem);
	}
    @Override
	public void initialize() {
		System.out.println("Before delay auto thing");
		

	}

	@Override
	public void execute() {
		// System.out.println("moving the robot");
		m_ArmSubsystem.retract();
	}

	@Override
	public void end(boolean interrupted) {
		// System.out.println("ending the moving the robot");
		m_ArmSubsystem.stopRetracting();
	}

	@Override
	public boolean isFinished() {
		
		return false;
    }
}
