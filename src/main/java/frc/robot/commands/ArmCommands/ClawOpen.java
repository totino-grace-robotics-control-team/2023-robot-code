package frc.robot.commands.ArmCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;

public class ClawOpen extends CommandBase{
	private final ArmSubsystem m_ArmSubsystem;
	public ClawOpen(ArmSubsystem subsystem) {
		//System.out.println("AutoDrive (command): Constructed");
		m_ArmSubsystem = subsystem;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_ArmSubsystem);
	}
    @Override
	public void initialize() {
		System.out.println("button is pressed");
		

	}

	@Override
	public void execute() {
		// System.out.println("moving the robot");
		//m_ArmSubsystem.open();
		//System.out.println("claw open is working");
		m_ArmSubsystem.open();
	}

	@Override
	public void end(boolean interrupted) {
		// System.out.println("ending the moving the robot");
		m_ArmSubsystem.stopMovingClawMotor();

		

	}

	@Override
	public boolean isFinished() {
		// m_ArmSubsystem.stopMovingClawMotor();
		// we're using whileTrue pattern to trigger an end

		return false;
    }
}
