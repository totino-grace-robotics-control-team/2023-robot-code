package frc.robot.commands.ArmCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ArmSubsystem;

public class ArmsDown extends CommandBase{
	private final ArmSubsystem m_ArmSubsystem;

	public ArmsDown(ArmSubsystem subsystem) {
		System.out.println("ArmsDown (command): Constructed");
		m_ArmSubsystem = subsystem;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_ArmSubsystem);
	}
    @Override
	public void initialize() {
		System.out.println("ArmsDown is 1, initialize");	
	}

	@Override
	public void execute() {
		System.out.println("ArmsDown is 2, execute");	
		// call moveDown;
		m_ArmSubsystem.moveDown();
	}

	@Override
	public void end(boolean interrupted) {
	System.out.println("ArmsDown is 3, end");	
	m_ArmSubsystem.stopMovingVertically();
	}

	@Override
	public boolean isFinished() {
	System.out.println("ArmsDown is 4, is finished");	
		//return m_ArmSubsystem.getVerticalLowerLimitSwitch();
		return false;
    }
	
}
