/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.Constants.DriveStickButtons;
//import frc.robot.Constants.FightStickButton;
import frc.robot.Constants.usbPortConstants;
// import frc.robot.commands.climber.*;
import frc.robot.commands.drivetrain.*;
import frc.robot.commands.ArmCommands.ClawOpen;
import frc.robot.commands.ArmCommands.TestNewCommands;
import frc.robot.commands.ArmCommands.ArmsDown;
import frc.robot.commands.ArmCommands.ArmsExtract;
import frc.robot.commands.ArmCommands.ArmsRetract;
import frc.robot.commands.ArmCommands.ArmsUp;
import frc.robot.commands.ArmCommands.ClawCloseCube;

// import frc.robot.commands.vision.*;
import frc.robot.subsystems.*;


/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in
 * the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of
 * the robot
 * (including subsystems, commands, and button mappings) should be declared
 * here.
 */

public class RobotContainer {
	AHRS m_navX = new AHRS(edu.wpi.first.wpilibj.I2C.Port.kOnboard); 

	// Subsystems
	// private final IntakeAndShoot m_intakeAndShoot = new IntakeAndShoot();
	// // TODO private final Elevator m_elevator = new Elevator();
	private final Drivetrain m_driveTrain = new Drivetrain(m_navX);
	private final ArmSubsystem m_ArmSubsystem = new ArmSubsystem();


	// private final CameraManager m_cameraManager = new CameraManager();

	// Controllers
	private final Joystick m_driveJoystick = new Joystick(usbPortConstants.kDriveJoystick);
	private final Joystick m_fightStick = new Joystick(usbPortConstants.kfightStick);

	// private final Joystick m_weaponsJoystick = new Joystick(usbPortConstants.kWeaponsJoystick);


	public void autonomousPeriodic() {
		// //  test code to see NavX values -- 
		// float pitch = m_navX.getPitch();
		// System.out.println("Nax: "+pitch);
	}

	// The container for the robot. Contains subsystems, OI devices, and commands.
	public RobotContainer() {

        // create the object to represent the NavX Sensor
		//m_navX = new AHRS(edu.wpi.first.wpilibj.I2C.Port.kOnboard); 
		m_navX.reset();

		// Set default drive command to joystick control
		m_driveTrain.setDefaultCommand(
				new RunCommand(() -> {
					m_driveTrain.drive(m_driveJoystick);
				}, m_driveTrain));

		  m_ArmSubsystem.setDefaultCommand(
		 		new RunCommand(() -> {
		 			m_ArmSubsystem.reachManual(m_fightStick);
		 		}, m_ArmSubsystem));

		// m_aim.setDefaultCommand(
		// new RunCommand(() -> {
		// m_aim.aimMotorManually(m_weaponsJoystick);
		// }, m_aim)
		// );


		// Configure button bindings, as defined below
		configureButtonBindings();

		// Add subsystems to SmartDashboard
		// SmartDashboard.putData(m_cwm);
		// SmartDashboard.putData(m_climber);
		SmartDashboard.putData(m_driveTrain);
		// SmartDashboard.putData(m_cameraManager);
	}

	// Define button mapping
	private void configureButtonBindings() {
		// Drive Controller - Drivetrain
		// Toggle polarity of Drivetrain controls
		final JoystickButton m_drivePolarityToggle = new JoystickButton(m_fightStick, DriveStickButtons.kToggleDriveButton);
		m_drivePolarityToggle.onTrue(new toggleDrive(m_driveTrain));

	/* 	final JoystickButton m_ArmsDownButton = new JoystickButton(m_driveJoystick, 3);
		final JoystickButton m_ArmsUpButton = new JoystickButton(m_driveJoystick, 4);
		final JoystickButton m_ArmsExtendButton = new JoystickButton(m_driveJoystick, 5);
		final JoystickButton m_ArmsRetractButton = new JoystickButton(m_driveJoystick, 6);
		final JoystickButton m_ClawCloseCubeButton = new JoystickButton(m_driveJoystick, 9);
		final JoystickButton m_ClawCloseConeButton = new JoystickButton(m_driveJoystick, 10);
		final JoystickButton m_ClawOpenButton = new JoystickButton(m_driveJoystick, 11);
		*/

		// below are button bindings for the fightstick -- enjoy!
		//final JoystickButton m_xButton = new JoystickButton(m_fightStick, 1); // pink
		//final JoystickButton m_aButton = new JoystickButton(m_fightStick, 2); // purple
		final JoystickButton m_bButton = new JoystickButton(m_fightStick, 3); //red
		final JoystickButton m_yButton = new JoystickButton(m_fightStick, 4); // cyan
		final JoystickButton m_L1Lb = new JoystickButton(m_fightStick, 5); // left gray
		final JoystickButton m_R1Rb = new JoystickButton(m_fightStick, 6); // right gray
		//final JoystickButton m_share = new JoystickButton(m_weaponsJoystick, 7); // share button
		// final JoystickButton m_options = new JoystickButton(m_fightStick, 10); // options

		// press a button, call this command
		//m_aButton.whileTrue(new ArmsDown(m_ArmSubsystem));
		m_bButton.whileTrue(new ArmsRetract(m_ArmSubsystem));
		//m_xButton.whileTrue(new ArmsUp(m_ArmSubsystem)); //extend and extract dont match
		m_yButton.whileTrue(new ArmsExtract(m_ArmSubsystem));
	//	m_L1Lb.whileTrue(new ArmsDown(m_ArmSubsystem));
		m_R1Rb.whileTrue(new ClawCloseCube(m_ArmSubsystem));
		m_L1Lb.whileTrue(new ClawOpen(m_ArmSubsystem));
		//m_options.whileTrue(new ClawOpen(m_ArmSubsystem));
	




		// m_drivePolarityToggle.whileHeld(new PunchOnTest(m_aim));
		// m_drivePolarityToggle.whenReleased(new PunchOffTest(m_aim));
		// final JoystickButton m_testCommandButton = new JoystickButton(m_fightStick,
		// 		Constants.FlightStickButtons.xButton);
		// m_testCommandButton.onTrue(new TestNewCommands(m_ArmSubsystem));

		// put a command button on smart dashboard
		SmartDashboard.putData("Test Command", new TestNewCommands(m_ArmSubsystem));
		SmartDashboard.putData("Up ramp command", new GoUpRamp((m_driveTrain)));
		SmartDashboard.putString("Claw open", "claw open");
		
		

		// SmartDashboard.putData("Autonomous Command", new
		// AutonomousCommandGroup(m_aim, m_intakeAndShoot, m_driveTrain));
		// SmartDashboard.putData("Release String", new ReleaseString(m_elevator));
		// SmartDashboard.putData("Tighten String", new TightenString(m_elevator));
		// SmartDashboard.putData("Aim Fixed", new AimFixed(m_aim, -25));
		// SmartDashboard.putData("Intake Ball", new IntakeBall(m_intakeAndShoot));


	}

	// Return the command to run in Autonomous Mode here
	public Command getAutonomousCommand() {
		// return new RunCommand(() -> {}); //run the command here, inside them brackets
		System.out.println("get autonomous command");
		DriverStation.getLocation();
		return new Autonomous2cmd(m_ArmSubsystem, m_driveTrain); // TODO: pass a real command

		// return new AutonomousCommandGroup(m_aim, m_intakeAndShoot, m_driveTrain);
		/**
		 * Since we'll be dong a lot, this should call a command group or process
		 * instead of the current anonymous function.
		 * 
		 * 
		 * Things to do in autonomous:
		 * 1. Start the Shooter
		 * 2. Wait for the Shooter to reach top speed
		 * (Potentially run a targeting routine here, depending on how long it takes to
		 * run)
		 * 3. Run conveyor until all 3 balls fired
		 * 4. Move off the Auto line
		 * (Potentially turn 180 here, to shave some time off the next step)
		 * 6. Look for yellow ball with vision
		 * 7. Intake yellow ball
		 * 8-11. Find and intake 2 more yellow balls
		 * (Potentially turn 180 here, to shave some time off the next step)
		 * 12. Target the retroreflective marks on the goal, using vision
		 * 13. Run conveyor to fire all 3 balls, again
		 */
	}
}